import React from "react";

export default function Header() {
  return (
    <div className="header bg-primary pt-lg-5 pb-lg-5">
      <h1 className="text-center">Header Component</h1>
    </div>
  );
}
