import React, { Component } from "react";

export default class Content extends Component {
  render() {
    return (
      <div
        className="item-right"
        style={{
          display: "flex",
          width: "70%",
          background: "blue",
          padding: 150,
          justifyContent: "center",
          alignItems: "center",
          verticalAlign: "middle",
        }}
      >
        <h1
          style={{
            margin: "auto",
          }}
        >
          Content Component
        </h1>
      </div>
    );
  }
}
