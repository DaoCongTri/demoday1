import React, { Component } from "react";

export default class DataBiding extends Component {
  imgUrl =
    "https://bazaarvietnam.vn/wp-content/uploads/2023/03/HBVN-rose-sang-trong-trong-chien-dich-moi-sulwhasoo.jpg";
  renderAge = () => {
    return <h1>Age: 2</h1>;
  };
  render() {
    // thuộc function render => dùng từ khóa let
    let userName = "Alice";
    return (
      <div>
        <h1>DataBinding</h1>
        <p>UserName: {userName}</p>
        <img src={this.imgUrl} alt="" height={500} />
        {this.renderAge()}
      </div>
    );
  }
}
