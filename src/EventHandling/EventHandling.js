import React, { Component } from "react";

export default class EventHandling extends Component {
  handleSayHello = () => {
    console.log("Hello User");
  };
  handleSayHelloWithName = (username) => {
    console.log(`Hello ${username}`);
  };
  render() {
    return (
      <div>
        <h1>Event Handling</h1>
        {/* ko dùng dấu () đối vs hàm ko có tham số*/}
        <button onClick={this.handleSayHello} className="btn btn-success">
          Say Hello
        </button>
        <br />
        {/* dùng arrow function đổi hàm có tham số */}
        <button
          onClick={() => {
            this.handleSayHelloWithName("ROSÉ");
          }}
        >
          Say Hi with name
        </button>
      </div>
    );
  }
}
