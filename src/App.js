// import logo from "./logo.svg";
import "./App.css";
// import Header from "./DemoComponent/Header";
// import Banner from "./DemoComponent/Banner";
// import NavTabs from "./DemoComponent/NavTabs";
import Carts from "./DemoComponent/Carts";
// import Header from "./ExLayout/Header";
// import Navigation from "./ExLayout/Navigation";
// import Content from "./ExLayout/Content";
// import Footer from "./ExLayout/Footer";
// import DataBiding from "./DataBinding/DataBiding";
// import EventHandling from "./EventHandling/EventHandling";
// import DemoState from "./DemoState/DemoState";
import ExStateCar from "./Ex_State_Car/ExStateCar";
function App() {
  return (
    <div className="App">
      {/* <Header />
      <Banner />
      <NavTabs /> */}
      {/* <Carts /> */}
      {/* <Header />
      <div style={{ display: "flex" }}>
        <Navigation />
        <Content />
      </div>
      <Footer />
      <div className="container">
        <DataBiding />
        <EventHandling />
        <DemoState />
      </div> */}
      <ExStateCar />
    </div>
  );
}

export default App;
